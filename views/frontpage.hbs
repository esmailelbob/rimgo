<!DOCTYPE html>
<html lang="en">

<head>
  <title>rimgo</title>

  {{> partials/head }}

  <link rel="stylesheet" href="/static/css/frontpage.css" />

</head>

<body>
  {{> partials/header }}

  <main>
    <p>An alternative frontend for Imgur. Originally based on <a href="https://codeberg.org/3np/rimgu">rimgu</a>.</p>

    <h3>Try it!</h3>

    <ul>
      <li><a href="/a/H8M4rcp">Album</a></li>
      <li><a href="/gallery/gYiQLWy">Gallery</a></li>
      <li><a href="/gallery/cTRwaJU">Video</a></li>
    </ul>

    <h2>Features</h2>

    <ul>
      <li>Lightweight</li>
      <li>No JavaScript</li>
      <li>No ads or tracking</li>
      <li>No sign up or app install prompts</li>
      <li>Bandwidth efficient - automatically uses newer image formats (if enabled)</li>
    </ul>

    <h2>Usage</h2>

    <p>Just replace imgur.com or i.imgur.com with <code>{{domain}}</code>! You can setup automatic redirects using <a
        href="https://github.com/libredirect/libredirect">LibRedirect</a> (recommended) or <a href="https://github.com/einaregilsson/Redirector">Redirector</a>.</p>
    <br>
    {{#if force_webp}}
    <p>To download images as their original filetype, add <code>?no_webp=1</code> to the end of the image URL.</p>
    {{/if}}

    <br>
    <details>
      <summary>
        Redirector configuration
      </summary>
      <ul>
          <li>Description: Imgur -> rimgo</li>
          <li>Example URL: https://imgur.com/a/H8M4rcp</li>
          <li>Include pattern: <code>^https?://i?.?imgur.com(/.*)?$</code></li>
          <li>Redirect to: <code>{{proto}}://{{domain}}$1</code></li>
          <li>Pattern type: Regular Expression</li>
        </ul>
    </details>
  </main>

  {{> partials/footer }}
</body>

</html>
